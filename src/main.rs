use std::io::{self, BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    // day_01_part_01()?;
    // day_01_part_02()?;
    // day_02_part_01()?;
    // day_02_part_02()?;
    day_03_part_01()?;

    Ok(())
}

#[allow(dead_code)]
fn day_03_part_01 () -> io::Result<()> {
    let mut file = File::open("src/input-03")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let wires: Vec<&str> = contents.lines().collect();

    let wire_a: Vec<&str> = wires[0].split(',').collect(); 
    let wire_b: Vec<&str> = wires[1].split(',').collect(); 

    let map: HashMap<(i32, i32), i32> = HashMap::new();

    fn process_wire (wire: Vec<&str>, map: HashMap<(i32, i32), i32>) {
        let mut point = (0, 0);
        for elem in wire {
            let direction = elem.chars().nth(0);
            let length = elem.parse::<i32>().unwrap();
            for n in 1..=length {
                println!("{}", n);
                match direction {
                    Some('U') => {
                        point = (point.0, point.1 + n);
                    },
                    Some('D') => {
                        point = (point.0, point.1 - n);
                    },
                    Some('R') => {
                        point = (point.0 + n, point.1);
                    },
                    Some('L') => {
                        point = (point.0 - n, point.1);
                    },
                    _ => {},
                }
                
            }
        }
    }

    Ok(())
}

#[allow(dead_code)]
fn day_02_part_02 () -> io::Result<()> {
    let mut file = File::open("src/input-02")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let default_codes: Vec<usize> = contents.split(',').map(|s| s.trim().parse().unwrap()).collect();

    let correct_output = 19690720;

    'noun: for noun in 0..100 {
        for verb in 0..100 {

            let mut codes = default_codes.to_vec();
            codes[1] = noun as usize;
            codes[2] = verb as usize;
            let mut i = 0;
            while i < codes.len() {
                let i_x = codes[i+1];
                let i_y = codes[i+2];
                let i_z = codes[i+3];

                match codes[i] {
                    1 => {
                        codes[i_z] = codes[i_x] + codes[i_y];
                        i += 4;
                    },
                    2 => {
                        codes[i_z] = codes[i_x] * codes[i_y];
                        i += 4;
                    },
                    99 => {
                        if codes[0] == correct_output {
                            println!("{}", 100 * noun + verb);
                            break 'noun;
                        }
                        break;
                    },
                    _ => {
                        println!("ERROR");
                        break;
                    },
                }
            }
        }
    }

    Ok(())
}

#[allow(dead_code)]
fn day_02_part_01 () -> io::Result<()> {
    let mut file = File::open("src/input-02")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let mut codes: Vec<usize> = contents.split(',').map(|s| s.trim().parse().unwrap()).collect();

    codes[1] = 12;
    codes[2] = 2;
    
    let mut i = 0;
    while i < codes.len() {
        let i_x = codes[i+1];
        let i_y = codes[i+2];
        let i_z = codes[i+3];

        match codes[i] {
            1 => {
                codes[i_z] = codes[i_x] + codes[i_y];
                i += 4;
            },
            2 => {
                codes[i_z] = codes[i_x] * codes[i_y];
                i += 4;
            },
            99 => {
                println!("HALT");
                break;
            },
            _ => {
                println!("ERROR");
                break;
            },
        }
    }

    println!("Value at position 0: {}", codes[0]);

    Ok(())
}

#[allow(dead_code)]
fn day_01_part_02 () -> io::Result<()> {
    fn calculate_fuel (mass: f64) -> f64 {
        let fuel: f64 = (mass / 3.0).floor() - 2.0;

        if fuel < 1.0 {
            return 0.0;
        } else {
            return fuel + calculate_fuel(fuel);
        }
    }

    let file = File::open("src/input-01")?;
    let reader = BufReader::new(file);

    let mut total_fuel: f64 = 0.0;
    for line in reader.lines() {
        let fuel = line.unwrap().parse::<f64>().unwrap();
        total_fuel += calculate_fuel(fuel);
    }
    println!("{}", total_fuel);

    Ok(())
}

#[allow(dead_code)]
fn day_01_part_01 () -> io::Result<()> {
    let file = File::open("src/input-01")?;
    let reader = BufReader::new(file);

    let mut total_fuel: i32 = 0;
    for line in reader.lines() {
        let fuel = line.unwrap().parse::<i32>().unwrap();
        total_fuel += (fuel / 3) - 2;
    }
    println!("{}", total_fuel);

    Ok(())
}

